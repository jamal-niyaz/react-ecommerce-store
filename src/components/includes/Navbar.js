import React, { Component } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { ButtonContainer } from "../partials/Button";

export default class Navbar extends Component {
  render() {
    return (
      <NavWrapper className="navbar navbar-expand-sm navbar-dark px-sm-5">
        <Link to="/">
          <img
            height="60"
            width="100"
            src={process.env.PUBLIC_URL + "/assets/images/nlabs.png"}
            alt="Store"
          />
        </Link>
        <ul className="navbar-nav align-items-center">
          <li className="nav-items ml-5">
            <Link to="/" className="nav-link">
              Store
            </Link>
          </li>
          <li className="nav-items ml-5">
            <Link to="/about" className="nav-link">
              About
            </Link>
          </li>
          <li className="nav-items ml-5">
            <Link to="/contact" className="nav-link">
              Contact
            </Link>
          </li>
        </ul>
        <Link to="/cart" className="ml-auto">
          <ButtonContainer>
            <span className="mr-2">
              <i className="fas fa-cart-plus"></i>
            </span>
            my cart
          </ButtonContainer>
        </Link>
      </NavWrapper>
    );
  }
}

const NavWrapper = styled.nav`
   {
    background: var(--mainBlue);
    .nav-link {
      color: var(--mainWhite) !important;
      font-size: 1rem;
      text-transform: Capitalize;
    }
  }
`;
