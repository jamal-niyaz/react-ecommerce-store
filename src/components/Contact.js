import React, { Component } from "react";

export default class Contact extends Component {
  render() {
    return (
      <div className="container">
        <div className="contact mt-5">
          <h1>Contact us</h1>
          <p className="lead">To reach us, niyaz.developer@gmail.com</p>
        </div>
      </div>
    );
  }
}
