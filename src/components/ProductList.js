import React, { Component } from 'react'
import Title from './includes/Title';
import Product from './Product';
import {ProductConsumer} from '../context';

export default class ProductList extends Component {
    // state = {
    //     products: storeProducts
    // };
    render() {
        return (
            <React.Fragment>
                <div className="py-5">
                    <div className="container">
                        <Title name="Flash" title="Sales" />
                        <div className="row">
                            <ProductConsumer>
                                {value => {
                                    return value.products.map(product => {
                                        return <Product key={product.id} product={product} />;
                                    })
                                }}
                            </ProductConsumer>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
