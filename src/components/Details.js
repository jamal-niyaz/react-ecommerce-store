import React, { Component } from 'react'
import {ProductConsumer} from '../context';
import {Link} from 'react-router-dom';
import {ButtonContainer} from './partials/Button';

export default class Details extends Component {
    render() {
        return (
            <ProductConsumer>
                {(value) => {
                    const {
                        id,
                        title,
                        info,
                        img,
                        price,
                        company,
                        inCart
                    } = value.detailProduct;
                    return (
                        <div className="container py-5">
                            {/* title  */}
                            <div className="row">
                                <div className="col-10 mx-auto text-center text-slanted text-blue my-2">
                                    <h5>{title}</h5>
                                </div>
                            </div>
                            {/* end title  */}

                            {/* product info  */}
                            <div className="row">
                                <div className="col-10 mx-auto col-md-6 my-2">
                                    <img src={img} className="img-fluid" alt="Product" />
                                </div>
                                <div className="col-10 mx-auto col-md-6 my-3">
                                    <h4>Model : {title}</h4>
                                    <h6 className="mt-3 mb-2">
                                        Made By: <span className="text-uppercase">{company}</span>
                                    </h6>
                                    <h6 className="mt-3 mb-2">
                                        Price: <span className="text-capitalize font-weight-medium">RM {price}</span>
                                    </h6>
                                    <p className="text-capitalize">Description</p>
                                    <p className="">{info}</p>
                                    {/* buttons */}
                                    <div>
                                        <Link to="/">
                                            <ButtonContainer>Back to products</ButtonContainer>
                                        </Link>
                                        <ButtonContainer
                                        cart
                                        disabled={inCart?true:false}
                                        onClick={()=>{
                                            value.addToCart(id);
                                            value.openModal(id);
                                        }}
                                        >
                                            {inCart? 'In Cart' : 'Add To cart'}
                                        </ButtonContainer>
                                    </div>
                                </div>
                            </div>
                            {/* end product info  */}

                        </div>
                    )
                }}
            </ProductConsumer>
        )
    }
}
