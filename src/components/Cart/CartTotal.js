import React from 'react'
import {Link} from 'react-router-dom'

export default function CartTotal({value}) {
    const {cartSubTotal, cartTax, cartTotal, clearCart} = value;
    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <div className="col-10 mt-2 ml-sm-5 ml-md-auto col-sm-8 text-capitalize text-right">
                        <Link to="/">
                            <button 
                                className="btn btn-outline-danger mb-3 px-5"
                                onClick={() => clearCart()}>
                                Clear Cart
                            </button>
                        </Link>
                        <h6><span>Subtotal : RM <strong>{cartSubTotal}</strong></span></h6>
                        <h6><span>Tax (10%) : RM  <strong>{cartTax}</strong></span></h6>
                        <h6><span>Shipping : RM  <strong>0</strong></span></h6>
                        <h5><span>Total : RM  <strong>{cartTotal}</strong></span></h5>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}
