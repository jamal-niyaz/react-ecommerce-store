import React, { Component } from 'react'
import Title from '../includes/Title';
import { ProductConsumer } from '../../context';
import CartColumns from './CartColumns';
import EmptyCart from './EmptyCart';
import CartList from './CartList';
import CartTotal from './CartTotal';

export default class Cart extends Component {
    render() {
        return (
            <div className="py-5">
                <div className="container-fluid mt-2">
                    <ProductConsumer>
                        {value => {
                            const {cart} = value;

                            if(cart.length > 0) {
                                return (
                                    <React.Fragment>
                                        <Title name="your" title="cart" />
                                        <CartColumns />
                                        <CartList value={value} />
                                        <CartTotal value={value} />
                                    </React.Fragment>
                                );
                            } else {
                                return (<EmptyCart />);
                            }
                        }}

                    </ProductConsumer>
                </div>
            </div>
        )
    }
}
