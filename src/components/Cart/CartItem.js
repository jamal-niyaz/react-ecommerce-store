import React from 'react'

export default function CartItem({item, value}) {

    const {id, title, img, price, total, count} = item;
    const {increment, decrement, removeItem} = value;

    return (
        <div className="container-fluid d-lg-block mt-2">
            <div className="row text-center text-capitalize">
                <div className="col-10 col-lg-2">
                    <img src={img} style={{ width: "5rem", height: "5rem" }} className="img-fluid" alt="Product" />
                </div>
                <div className="col-10 col-lg-2">
                    {title}
                </div>
                <div className="col-10 col-lg-2">
                    RM {price}
                </div>
                <div className="col-10 col-lg-2">
                    <div className="d-flex justify-content-center">
                        <span className="btn btn-black mx-1" onClick={() => decrement(id)}>-</span>
                        <span className="btn btn-black mx-1">{count}</span>
                        <span className="btn btn-black mx-1" onClick={() => increment(id)}>+</span>
                    </div>
                </div>
                <div className="col-10 col-lg-2 my-2 my-lg-0">
                    <div className="cart-icon" onClick={()=>removeItem(id)}>
                        <i className="fas fa-trash"></i>
                    </div>
                </div>
                <div className="col-10 col-lg-2">
                    <span className="">RM {total}</span>
                </div>
            </div>
        </div>
    )
}
