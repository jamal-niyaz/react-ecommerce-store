import React, { Component } from "react";

export default class About extends Component {
  render() {
    return (
      <div className="container">
        <div className="starter-template mt-5">
          <h1>About Store</h1>
          <p className="lead">
            E-Store development is in alpha. Thanks for visting.
          </p>
        </div>
      </div>
    );
  }
}
